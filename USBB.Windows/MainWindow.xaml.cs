﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml.Serialization;
using USBB.Domain.ViewModel;
using USBB.Model;

namespace USBB.Windows
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ClearDefaultTemplateButton_Click(object sender, RoutedEventArgs e)
        {
            DefaultTemplateComboBox.SelectedItem = null;
        }

        private void TemplatesList_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(TemplatesList, e.OriginalSource as DependencyObject);
            if (item != null)
            {
                var template = (item as ListBoxItem).DataContext as string;
                var dc = DataContext as IMainViewModel;
                dc.AddAllowedTemplate.Execute(template);
            }
        }

        private void ClassPreview_Loaded(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as IMainViewModel;
            dc.UpdatePreview.Execute(null);
        }

        private void ClearParentDoctypeButton_Click(object sender, RoutedEventArgs e)
        {
            ParentDoctypeComboBox.SelectedItem = null;
        }

        private void DoctypesList_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(DoctypesList, e.OriginalSource as DependencyObject);
            if (item != null)
            {
                var doctype = (item as ListBoxItem).DataContext as DocumentType;
                var allowedChildDoctypes = AllowedChildDoctypesList.DataContext as List<DocumentType>;
                var dc = DataContext as IMainViewModel;
                dc.AddAllowedChildNode.Execute(doctype);
            }
        }

        private void AllowedChildDoctypesList_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(AllowedChildDoctypesList, e.OriginalSource as DependencyObject);
            if (item != null)
            {
                var doctype = (item as ListBoxItem).DataContext as string;
                var allowedChildDoctypes = AllowedChildDoctypesList.DataContext as List<DocumentType>;
                var dc = DataContext as IMainViewModel;
                dc.RemoveAllowedChildNode.Execute(doctype);
            }
        }

        private void AllowedTemplatesList_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(AllowedTemplatesList, e.OriginalSource as DependencyObject);
            if (item != null)
            {
                var template = (item as ListBoxItem).DataContext as string;
                var dc = DataContext as IMainViewModel;
                dc.RemoveAllowedTemplate.Execute(template);
            }
        }

        private void SaveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.DefaultExt = ".xml";
            sfd.Filter = "XML file|*.xml";
            sfd.FileOk += Sfd_FileOk;
            sfd.ShowDialog();
        }

        private void Sfd_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(ObservableCollection<DocumentType>));
                using (var writer = new StreamWriter((sender as SaveFileDialog).FileName))
                {
                    serializer.Serialize(writer, (DataContext as IMainViewModel).DocumentTypes);
                }
            }
            catch (IOException ex)
            {
                System.Windows.MessageBox.Show("An error occured while saving file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "XML files|*.xml";
            ofd.DefaultExt = ".xml";
            var result = ofd.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(ObservableCollection<DocumentType>));
                    using (var reader = new StreamReader(ofd.FileName))
                    {
                        (DataContext as IMainViewModel).DocumentTypes = serializer.Deserialize(reader) as ObservableCollection<DocumentType>;
                    }
                }
                catch (InvalidOperationException ex)
                {
                    System.Windows.MessageBox.Show("Invalid XML in file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (IOException ex)
                {
                    System.Windows.MessageBox.Show("An error occured while opening file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private async void ExportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var fbd = new FolderBrowserDialog();
            var result = fbd.ShowDialog();
            var dc = DataContext as IMainViewModel;

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                foreach (var doctypeInfo in dc.GetAllDoctypesForExport())
                {
                    try
                    {
                        using (var writer = new StreamWriter(fbd.SelectedPath + "\\" + doctypeInfo.Item2.Identifier + ".cs"))
                        {
                            await writer.WriteAsync(doctypeInfo.Item1);
                        }
                    }
                    catch (IOException)
                    {
                        System.Windows.MessageBox.Show("Could not save the files - an error occured", "IO Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    catch (UnauthorizedAccessException)
                    {
                        System.Windows.MessageBox.Show("Could not save the files - access is denied", "Access denied", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
    }
}

