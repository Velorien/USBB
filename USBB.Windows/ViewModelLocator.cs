﻿using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USBB.Domain.Logic;
using USBB.Domain.ViewModel;
using USBB.Logic;
using USBB.ViewModel;

namespace USBB.Windows
{
    public class ViewModelLocator
    {
        private IUnityContainer container;

        public ViewModelLocator()
        {
            container = new UnityContainer();

            container.RegisterType<IMainViewModel, MainViewModel>(new ContainerControlledLifetimeManager());
            container.RegisterType<IUmbracoConnection, UmbracoConnection>();
            container.RegisterType<IDialogService, DialogService>();
            container.RegisterType<IDoctypeClassGenerator, DoctypeClassGenerator>(new ContainerControlledLifetimeManager());
        }

        public IMainViewModel MainViewModel
        {
            get { return container.Resolve<IMainViewModel>(); }
        }
    }
}
