﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using USBB.Domain.Logic;
using USBB.Domain.ViewModel;
using USBB.Model;

namespace USBB.ViewModel
{
    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private ObservableCollection<DocumentType> documentTypes;
        private DocumentType selectedDocumentType;
        private DocumentTypeProperty selectedProperty;
        private string documentTypeClass;
        private string connectionString;
        private List<string> templates;
        private List<string> propertyTypes;
        private List<string> sqlEngines;

        private IUmbracoConnection connection;
        private IDialogService dialogService;
        private IDoctypeClassGenerator doctypeClassGenerator;

        public MainViewModel(IUmbracoConnection connection, IDialogService dialogService, IDoctypeClassGenerator doctypeClassGenerator)
        {
            this.connection = connection;
            this.dialogService = dialogService;
            this.doctypeClassGenerator = doctypeClassGenerator;

            DocumentTypes = new ObservableCollection<DocumentType>();

            Connect = new RelayCommand<string>(ConnectExecute);
            AddDoctype = new RelayCommand(() => DocumentTypes.Add(new DocumentType() { Identifier = "NewDocumentType" }));
            DeleteDoctype = new RelayCommand(() => DocumentTypes.Remove(SelectedDocumentType));
            AddProperty = new RelayCommand(() => SelectedDocumentType.Properties.Add(new DocumentTypeProperty() { Identifier = "NewProperty" }));
            DeleteProperty = new RelayCommand(() => SelectedDocumentType.Properties.Remove(SelectedProperty));
            UpdatePreview = new RelayCommand(() => DocumentTypeClass = this.doctypeClassGenerator.Generate(SelectedDocumentType));
            AddAllowedTemplate = new RelayCommand<string>(x => { if (SelectedDocumentType != null && !SelectedDocumentType.AllowedTemplates.Contains(x)) { SelectedDocumentType.AllowedTemplates.Add(x); } });
            RemoveAllowedTemplate = new RelayCommand<string>(x => SelectedDocumentType.AllowedTemplates.Remove(x));
            AddAllowedChildNode = new RelayCommand<DocumentType>(x => { if (SelectedDocumentType != null && !SelectedDocumentType.AllowedChildNodeTypes.Contains(x.Identifier)) { SelectedDocumentType.AllowedChildNodeTypes.Add(x.Identifier); } });
            RemoveAllowedChildNode = new RelayCommand<string>(x => SelectedDocumentType.AllowedChildNodeTypes.Remove(x));

            SqlEngines = this.connection.SupportedEngines;
            PropertyTypes = new List<string>()
            {
                "ApprovedColor", "CheckboxList", "ContentPicker", "DatePicker", "DatePickerWithTime",
                "Dropdown", "DropdownMultiple", "FolderBrowser", "Label", "MediaPicker", "MemberPicker",
                "Numeric", "Other", "Radiobox", "RelatedLinks", "RichtextEditor", "SimpleEditor", "Tags",
                "TextboxMultiple", "Textstring", "TrueFalse", "UltimatePicker", "Upload"
            };
        }

        public ObservableCollection<DocumentType> DocumentTypes
        {
            get { return documentTypes; }
            set { Set(() => DocumentTypes, ref documentTypes, value); }
        }

        public DocumentType SelectedDocumentType
        {
            get { return selectedDocumentType; }

            set
            {
                Set(() => SelectedDocumentType, ref selectedDocumentType, value);
                UpdatePreview.Execute(null);
            }
        }

        public DocumentTypeProperty SelectedProperty
        {
            get { return selectedProperty; }

            set { Set(() => SelectedProperty, ref selectedProperty, value); }
        }

        public string DocumentTypeClass
        {
            get { return documentTypeClass; }

            set { Set(() => DocumentTypeClass, ref documentTypeClass, value); }
        }

        public string ConnectionString
        {
            get { return connectionString; }

            set
            {
                Set(() => ConnectionString, ref connectionString, value);
                if (connection != null)
                {
                    connection.ConnectionString = value;
                }
            }
        }

        public List<string> Templates
        {
            get { return templates; }

            set { Set(() => Templates, ref templates, value); }
        }

        public List<string> PropertyTypes
        {
            get { return propertyTypes; }

            set { Set(() => PropertyTypes, ref propertyTypes, value); }
        }

        public List<string> SqlEngines
        {
            get { return sqlEngines; }

            set { Set(() => SqlEngines, ref sqlEngines, value); }
        }

        public ICommand Connect { get; private set; }

        public ICommand AddDoctype { get; private set; }

        public ICommand DeleteDoctype { get; private set; }

        public ICommand AddProperty { get; private set; }

        public ICommand DeleteProperty { get; private set; }

        public ICommand UpdatePreview { get; private set; }

        public ICommand AddAllowedTemplate { get; private set; }

        public ICommand RemoveAllowedTemplate { get; private set; }

        public ICommand AddAllowedChildNode { get; private set; }

        public ICommand RemoveAllowedChildNode { get; private set; }

        public List<Tuple<string, DocumentType>> GetAllDoctypesForExport()
        {
            var doctypeInfo = new List<Tuple<string, DocumentType>>();
            foreach(var doctype in DocumentTypes)
            {
                doctypeInfo.Add(new Tuple<string, DocumentType>(doctypeClassGenerator.Generate(doctype), doctype));
            }

            return doctypeInfo;
        }

        private async void ConnectExecute(string sqlEngine)
        {
            connection.ConnectionString = ConnectionString;

            Templates = await connection.GetTemplatesAsync(sqlEngine);

            if (Templates == null)
            {
                await dialogService.ShowMessage("Could not connect to the database", "Connection error");
            }
        }
    }
}
