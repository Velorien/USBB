﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace USBB.Domain.Logic
{
    public interface IUmbracoConnection
    {
        string ConnectionString { get; set; }

        List<string> SupportedEngines { get; set; }

        Task<List<string>> GetTemplatesAsync(string engine);
    }
}
