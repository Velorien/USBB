﻿using USBB.Model;

namespace USBB.Domain.Logic
{
    public interface IDoctypeClassGenerator
    {
        string Generate(DocumentType model);
    }
}
