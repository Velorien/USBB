﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using USBB.Model;

namespace USBB.Domain.ViewModel
{
    public interface IMainViewModel
    {
        ObservableCollection<DocumentType> DocumentTypes { get; set; }

        DocumentType SelectedDocumentType { get; set; }

        DocumentTypeProperty SelectedProperty { get; set; }

        string DocumentTypeClass { get; set; }

        string ConnectionString { get; set; }

        List<string> Templates { get; set; }

        List<string> PropertyTypes { get; set; }

        List<string> SqlEngines { get; set; }

        ICommand Connect { get; }

        ICommand AddDoctype { get; }

        ICommand DeleteDoctype { get; }

        ICommand AddProperty { get; }

        ICommand DeleteProperty { get; }

        ICommand UpdatePreview { get; }

        ICommand AddAllowedTemplate { get; }

        ICommand RemoveAllowedTemplate { get; }

        ICommand AddAllowedChildNode { get; }

        ICommand RemoveAllowedChildNode { get; }

        List<Tuple<string, DocumentType>> GetAllDoctypesForExport();
    }
}
