# uSiteBuilder Builder
A simple [uSiteBuilder](http://usitebuilder.vegaitsourcing.rs/) class generator
## Features
* connect to MySQL or SQL Server database to fetch available templates
* create and browse classes and properties with a friendly editor
* export and import your workspace as one xml file
* batch export created document type classes