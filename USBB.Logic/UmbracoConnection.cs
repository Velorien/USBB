﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;
using USBB.Domain.Logic;

namespace USBB.Logic
{
    public class UmbracoConnection : IUmbracoConnection
    {
        private DbConnection connection;

        public string ConnectionString { get; set; }

        public List<string> SupportedEngines { get; set; } = new List<string> { "MySQL", "SQL Server" };

        public async Task<List<string>> GetTemplatesAsync(string engine)
        {
            if(await OpenConnectionAsync(engine))
            {
                var templates = new List<string>();

                if (connection is MySqlConnection)
                {
                    var command = new MySqlCommand("select alias from cmstemplate");
                    command.Connection = connection as MySqlConnection;
                    var reader = await command.ExecuteReaderAsync();

                    while (await reader.ReadAsync())
                    {
                        templates.Add(reader["alias"] as string);
                    }
                }
                else
                {
                    var command = new SqlCommand("select alias from cmstemplate");
                    command.Connection = connection as SqlConnection;
                    var reader = await command.ExecuteReaderAsync();

                    while (await reader.ReadAsync())
                    {
                        templates.Add(reader["alias"] as string);
                    }
                }

                CloseConnection();
                return templates;
            }

            return null;
        }

        private async Task<bool> OpenConnectionAsync(string engine)
        {
            try
            {
                if (engine.Equals(SupportedEngines[0]))
                {
                    connection = new MySqlConnection();
                }
                else
                {
                    connection = new SqlConnection();
                }

                connection.ConnectionString = ConnectionString;
                await connection.OpenAsync();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
            catch (SqlException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        private void CloseConnection()
        {
            if(connection != null)
            {
                connection.Close();
            }
        }
    }
}
