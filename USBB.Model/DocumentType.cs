﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USBB.Model
{
    public class DocumentType
    {
        public DocumentType()
        {
            AllowedChildNodeTypes = new ObservableCollection<string>();
            AllowedTemplates = new ObservableCollection<string>();
            Properties = new ObservableCollection<DocumentTypeProperty>();
        }

        public ObservableCollection<string> AllowedChildNodeTypes { get; set; }

        public ObservableCollection<string> AllowedTemplates { get; set; }

        public string Identifier { get; set; }

        public string DefaultTemplate { get; set; }

        public string Description { get; set; }

        public string IconUrl { get; set; }

        public string Name { get; set; }

        public string Thumbnail { get; set; }

        public string Alias { get; set; }

        public bool AllowAtRoot { get; set; }

        public ObservableCollection<DocumentTypeProperty> Properties { get; set; }

        public string Namespace { get; set; }

        public DocumentType ParentDocumentType { get; set; }
    }
}
