﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USBB.Model
{
    public class DocumentTypeProperty
    {
        public string UmbracoPropertyType { get; set; }

        public string Identifier { get; set; }

        public string DefaultValue { get; set; }

        public string Description { get; set; }

        public bool Mandatory { get; set; }

        public string Name { get; set; }

        public string OtherTypeName { get; set; }

        public string Tab { get; set; }

        public string ValidationRegExp { get; set; }

        public string Alias { get; set; }
    }
}
